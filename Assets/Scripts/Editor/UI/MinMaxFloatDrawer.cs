﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(MinMaxFloat))]
public class MinMaxFloatDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		float nameWidth = position.width * .41f;

		float labelWidth = 50f;
		float fieldWidth = ((position.width - nameWidth) / 2f) - labelWidth;

		SerializedProperty min = property.FindPropertyRelative("min");
		SerializedProperty max = property.FindPropertyRelative("max");

		float posx = position.x;

		EditorGUI.LabelField(new Rect(position.x, position.y, nameWidth, position.height), property.displayName);
		posx += nameWidth;

		// Draw Min
		EditorGUI.LabelField(new Rect(posx, position.y, labelWidth, position.height), "Min");
		posx += labelWidth;
		min.floatValue = EditorGUI.FloatField(new Rect(posx, position.y, fieldWidth, position.height), min.floatValue);
		posx += fieldWidth;

		// Draw Max
		EditorGUI.LabelField(new Rect(posx, position.y, labelWidth, position.height), "Max");
		posx += labelWidth;
		max.floatValue = EditorGUI.FloatField(new Rect(posx, position.y, fieldWidth, position.height), max.floatValue);
	}
}