﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : SingletonMB<Player>
{
	[Header("Ball")]
	[SerializeField] private BallPlayer m_LeftBall = null;
	[SerializeField] private BallPlayer m_RightBall = null;

	[Header("Movement")]
	[SerializeField] private float m_SpeedMove = 10;
	[SerializeField] private float m_MinXMovement = 0.1f;
	[SerializeField] private float m_MaxXMovement = 1;


	public bool m_CanMove { get; private set; }
	public float m_CurSpeed { get; private set; }

	private bool m_tapDown;
	private float m_DefaultYPos;

	protected override void Awake()
    {
        base.Awake();
		m_DefaultYPos = this.transform.position.y;

		StateManager.Instance.OnGameStateChanged += OnGameStateChanged;
    }

	private void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.MENU:
                Init();
                break;
            case GameState.GAME:
				StartRun();
				break;
            case GameState.SUCCESS:
				StopRun(true);
				break;
            case GameState.FAIL:
				StopRun(false);
                break;
		}
    }

    private void OnDestroy()
    {
        if ( StateManager.Instance != null )
            StateManager.Instance.OnGameStateChanged -= OnGameStateChanged;
    }

    private void Init()
    {
		if (seq != null && !seq.IsActive())
		{
			seq.Kill();
		}
		if (seq == null || !seq.IsActive())
		{
			seq = DOTween.Sequence();
			seq.Append(this.transform.DOLocalMoveY(m_DefaultYPos + 0.1f, 1)
				.OnComplete(() => this.transform.DOLocalMoveY(m_DefaultYPos, 1)));
			seq.SetLoops(-1, LoopType.Yoyo);

		}

		ResetPlayer();
	}

	private void ResetPlayer()
	{
		m_LeftBall.ResetBall();
		m_RightBall.ResetBall();

		Vector3 posLeftBall = m_LeftBall.transform.position;
		Vector3 posRightBall = m_RightBall.transform.position;
		posLeftBall.x = -m_MinXMovement;
		posRightBall.x = m_MinXMovement;

		deltaPosition = m_MinXMovement;
		wantedDeltaPosition = m_MinXMovement;

		m_LeftBall.transform.position = posLeftBall;
		m_RightBall.transform.position = posRightBall;
	}

	void Update()
    {
		if (StateManager.Instance.CurrentState != GameState.GAME)
            return;

#if UNITY_EDITOR
		HandleKeyboardInput();
#else
        HandleTouchInput();
#endif

		UpdateDeltaPosition();
	}

	private void StartRun()
	{
		m_CanMove = true;
		m_CurSpeed = 1;

		if (seq != null && !seq.IsActive())
		{
			seq.Kill();
		}
	}

	Sequence seq;

	private void StopRun(bool victory)
	{
		m_CanMove = false;

		m_CurSpeed = 1;
		DOTween.To(() => m_CurSpeed, x => m_CurSpeed = x, 0, 1);

		if (seq != null && !seq.IsActive())
		{
			seq.Kill();
		}
		if (seq == null || !seq.IsActive())
		{
			seq = DOTween.Sequence();
			seq.Append(this.transform.DOLocalMoveY(m_DefaultYPos + 0.1f, 1)
				.OnComplete(() => this.transform.DOLocalMoveY(m_DefaultYPos, 1)));

			seq.SetLoops(-1, LoopType.Yoyo);
		}
	}

	#region input
	private void HandleKeyboardInput()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			ResolveButtonDown(Input.mousePosition);
		}

		if (Input.GetButton("Fire1"))
		{
			ResolveButtonHold(Input.mousePosition);
		}
		else
		{
			ResolveButtonUp(Input.mousePosition);
		}
	}

	private void HandleTouchInput()
	{
		if (Input.touchCount == 0)
			return;

		Touch t = Input.GetTouch(0);

		switch (t.phase)
		{
			case TouchPhase.Began:
				ResolveButtonDown(t.position);
				break;
			case TouchPhase.Moved:
			case TouchPhase.Stationary:
				ResolveButtonHold(t.position);
				break;
			default:
				ResolveButtonUp(t.position);
				break;
		}
	}


	private Vector3 prevPositionInput;
	private float wantedDeltaPosition;
	private float deltaPosition;


	private void ResolveButtonDown(Vector3 positionInput)
	{
		m_tapDown = true;
		prevPositionInput = ConvertInputToWorldPosition(positionInput);
		// HoldBegin();
	}

	private Vector3 ConvertInputToWorldPosition(Vector3 positionInput)
	{
		Vector3 camPos = Camera.main.transform.position;
		Vector3 ballPos = m_RightBall.transform.position;

		float dist = Vector3.Distance(camPos, ballPos);
		positionInput.z = dist;

		return Camera.main.ScreenToWorldPoint(positionInput);
	}

	private void ResolveButtonHold(Vector3 positionInput)
	{
		m_tapDown = true;

		positionInput = ConvertInputToWorldPosition(positionInput);
		Vector3 deltaPositionInput = positionInput - prevPositionInput;

		wantedDeltaPosition = Mathf.Clamp(wantedDeltaPosition + deltaPositionInput.x, m_MinXMovement, m_MaxXMovement);

		prevPositionInput = positionInput;
	}

	private void ResolveButtonUp(Vector3 positionInput)
	{
		if (m_tapDown)
		{
			// HoldEnd();
			m_tapDown = false;
		}
	}

	//float m_CurSpeed2;

	private void UpdateDeltaPosition()
	{
		//deltaPosition = wantedDeltaPosition;
		deltaPosition = Mathf.Lerp(deltaPosition, wantedDeltaPosition, Time.deltaTime * m_SpeedMove);
		//deltaPosition = Mathf.SmoothDamp(deltaPosition, wantedDeltaPosition, ref m_CurSpeed2, .2f);

		Vector3 rightPos = this.m_RightBall.transform.position;
		rightPos.x = deltaPosition;
		this.m_RightBall.transform.position = rightPos;

		Vector3 leftPos = this.m_LeftBall.transform.position;
		leftPos.x = -deltaPosition;
		this.m_LeftBall.transform.position = leftPos;
	}
	#endregion
}