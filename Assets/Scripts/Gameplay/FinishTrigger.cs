﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTrigger : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{

			BallPlayer ballPlayer = other.GetComponent<BallPlayer>();
			if (ballPlayer != null)
			{
				if (StateManager.Instance.CurrentState == GameState.GAME)
					StateManager.Instance.ChangeState(GameState.SUCCESS);
			}
		}
	}
}
