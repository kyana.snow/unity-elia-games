﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPlayer : MonoBehaviour
{
	[SerializeField] private int m_IdCorrectColor = 0;

	[SerializeField] private GameObject m_modelBall = null;
	[SerializeField] private float m_timeBounce = 0.2f;
	[SerializeField] private float m_ScaleBounce = 0.2f;
	[SerializeField] private GameObject m_PrefabExplose = null;

	Sequence seq;

	public void ResetBall()
	{
		m_modelBall.transform.localScale = new Vector3(1, 1, 1);
	}

	public int GetIdColor()
	{
		return m_IdCorrectColor;
	}

	public void PassColor(bool good)
	{
		if (good)
		{
			MobileHapticManager.Instance.HapticLightImpact();
		}
		else
		{
			MobileHapticManager.Instance.HapticMediumImpact();
		}

		if (seq == null || !seq.IsActive())
		{
			seq = DOTween.Sequence();
			seq.Append(m_modelBall.transform.DOPunchScale(new Vector3(m_ScaleBounce, m_ScaleBounce, 0.1f), m_timeBounce).OnComplete(() => seq.Kill()));
		}
	}

	public void PassObstacle()
	{
		MobileHapticManager.Instance.HapticMediumImpact();

	}

	public void PlayerDie()
	{
		MobileHapticManager.Instance.HapticFailure();
		if (seq != null && seq.IsActive())
		{
			seq.Kill();
		}

		m_modelBall.transform.DOScale(new Vector3(0.01f, 0.01f, 0.01f), 0.1f).SetEase(Ease.InFlash);
		Instantiate(m_PrefabExplose, this.transform);
	}
}
