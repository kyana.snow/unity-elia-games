﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTrigger : MonoBehaviour
{
	[SerializeField] private ObstacleTrigger m_Parent = null;
	[SerializeField] private GameObject m_Cube = null;
	[SerializeField] private Color m_ColorGood = Color.green;
	[SerializeField] private float m_timeEffectBounce = 0.2f;
	[SerializeField] private float m_timeEffectColor = 0.2f;
	[SerializeField] private float m_PunchScale = 0.2f;

	private bool m_IsDone;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			if (m_IsDone)
				return;

			BallPlayer ballPlayer = other.GetComponent<BallPlayer>();
			if (ballPlayer != null)
			{
				if (m_Parent != null)
				{
					ballPlayer.PassObstacle();
					m_Parent.PassObstacle();
				}
				else
				{
					ballPlayer.PlayerDie();
					CollidObstacle();
				}
			}
		}
	}

	void PassObstacle()
	{
		m_IsDone = true;

		Renderer rend = m_Cube.transform.GetComponent<Renderer>();

		m_Cube.transform.DOPunchScale(new Vector3(m_PunchScale, m_PunchScale, 0f), m_timeEffectBounce);
		rend.material.DOColor(m_ColorGood, m_timeEffectColor);
	}

	void CollidObstacle()
	{
		//m_IsDone = true;

		if (StateManager.Instance.CurrentState == GameState.GAME)
			StateManager.Instance.ChangeState(GameState.FAIL);
	}
}
