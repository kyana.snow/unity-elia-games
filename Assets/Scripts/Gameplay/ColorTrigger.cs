﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTrigger : MonoBehaviour
{
	[SerializeField] private int m_IdColor = 0;
	[SerializeField] private GameObject m_Cube = null;
	[SerializeField] private float m_timeEffect = 0.2f;
	[SerializeField] private Color m_ColorGood = Color.white;
	[SerializeField] private Color m_ColorBad = Color.grey;

	private bool m_IsDone;
	private Color m_ColorBase;

	void OnTriggerEnter(Collider other)
	{
		if (m_IsDone)
			return;

		if (other.gameObject.tag == "Player")
		{
			BallPlayer ballPlayer = other.GetComponent<BallPlayer>();
			if (ballPlayer != null)
			{
				if (ballPlayer.GetIdColor() == m_IdColor)
				{
					ballPlayer.PassColor(true);
					DoFX(true);
				}
				else
				{
					ballPlayer.PassColor(false);
					DoFX(false);
				}
			}
		}
	}

	void DoFX(bool good)
	{
		m_IsDone = true;

		Renderer rend = m_Cube.transform.GetComponent<Renderer>();
		m_ColorBase = rend.material.color;


		m_Cube.transform.DOPunchScale(new Vector3(0.18f, 0.23f, 0f), m_timeEffect);

		if (good)
		{
			rend.material.DOColor(m_ColorGood, m_timeEffect).OnComplete(() => rend.material.DOColor(m_ColorBase, m_timeEffect));
		}
		else
		{
			rend.material.DOColor(m_ColorBad, m_timeEffect).OnComplete(() => rend.material.DOColor(m_ColorBase, m_timeEffect));
		}
		
	}
}
