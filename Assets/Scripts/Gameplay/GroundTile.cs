﻿using Assets.Scripts.Water;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTile : MonoBehaviour
{
	[SerializeField] private Transform m_StartPoint = null;
	[SerializeField] private Transform m_EndPoint = null;
	[SerializeField] private Transform m_SpawnPrefab = null;

	private GameObject m_Obstacle;
	private Renderer[] renderers;
	private WaterPropertyBlockSetter blockSetter;

	public void Init(GameObject prefab, WaterPropertyBlockSetter blockSetter)
	{
		this.blockSetter = blockSetter;

		if (m_Obstacle != null)
		{
			Destroy(m_Obstacle);
		}

		if (prefab != null)
		{
			GameObject spawnedObstacle = Instantiate(prefab, m_SpawnPrefab);
			spawnedObstacle.transform.localPosition = Vector3.zero;
			m_Obstacle = spawnedObstacle;

			renderers = spawnedObstacle.GetComponentsInChildren<Renderer>();

			SetPropertyBlock();
		}
	}

	private void Update()
	{
		SetPropertyBlock();
	}

	private void SetPropertyBlock()
	{
		if (renderers != null)
		{
			foreach (Renderer r in renderers)
			{
				if (r)
				{
					r.SetPropertyBlock(blockSetter.MaterialPropertyBlock);
				}
				
			}
		}
	}

	#region Get
	public Transform GetStartPoint()
	{
		return m_StartPoint;
	}

	public Transform GetEndPoint()
	{
		return m_EndPoint;
	}
	#endregion
}
