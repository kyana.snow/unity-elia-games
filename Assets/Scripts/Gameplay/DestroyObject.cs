﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
	[SerializeField] private bool m_Destroy = false;
	[SerializeField] private float m_timer = 0;

	void OnEnable()
    {
		if (m_Destroy)
		{
			Invoke(nameof(DistroyObjectFct), m_timer);
		}
	}

	void DistroyObjectFct()
	{
		this.gameObject.SetActive(false);
	}
}
