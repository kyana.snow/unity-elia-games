﻿using UnityEngine;

[System.Serializable]
public class MinMaxFloat
{
    public float min;
    public float max;

    public float value
    {
        get { return Random.Range(min,max); }
    }

    public MinMaxFloat(float newMin, float newMax)
    {
        min = newMin;
        max = newMax;
    }
}