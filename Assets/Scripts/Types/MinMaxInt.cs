﻿using UnityEngine;

[System.Serializable]
public class MinMaxInt
{
    public int min;
    public int max;

    public int value
    {
        get { return Random.Range(min, max); }
    }

    public MinMaxInt(int newMin, int newMax)
    {
        min = newMin;
        max = newMax;
    }
}