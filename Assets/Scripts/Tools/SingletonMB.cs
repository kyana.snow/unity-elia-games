﻿using UnityEngine;

public class SingletonMB<T> : MonoBehaviour where T : MonoBehaviour
{

    [SerializeField]
    private bool m_persistAcrossScene = false;


    private static T _instance;

    //private static object _lock = new object();

    public static T Instance
    {
        get
        {
            //lock(_lock)
			//{
				if (_instance == null)
				{
                    _instance = (T)FindObjectOfType(typeof(T));
				}
                
				return _instance;
			//}
        }
    }


    protected virtual void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this as T;

            if (m_persistAcrossScene)
            {
                transform.parent = null;
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}