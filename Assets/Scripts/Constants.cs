﻿// Game States list for StateManager
// edit to add your own states
public enum GameState
{
    MENU = 0,
    GAME = 1,
    SUCCESS = 2,
    FAIL = 3
}

// View names for the UI Manager
// edit to set up your own views
public enum UIView
{
    MAIN_MENU = 0,
    IN_GAME = 1,
    SUCCESS = 2,
    FAIL = 3,
}

// ids of the particles in the fx manager
// edit to set up your own
public enum ParticleId
{
    EXPLOSION,
    FIREWORK,
}

// ids of the sounds in the sound manager
// edit to set up your own
public enum SoundId
{
   
}

public class Constants
{

}