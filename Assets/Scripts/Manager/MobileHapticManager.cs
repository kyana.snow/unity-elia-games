using System.Collections.Generic;
using UnityEngine;
#if MOREMOUNTAINS_NICEVIBRATIONS
using MoreMountains.NiceVibrations;
#endif

public class MobileHapticManager : SingletonMB<MobileHapticManager>
{
    public bool m_verbose = false;  
    private bool m_canVibrate = true; 
   
    private void Start()
    {

#if MOREMOUNTAINS_NICEVIBRATIONS
        m_canVibrate = MMVibrationManager.HapticsSupported();
#endif

#if UNITY_EDITOR
            m_canVibrate = false;
#endif

        

#if UNITY_IOS
        if (!m_canVibrate)
        {
            LogMessage("Device older than Iphone 7. No haptic vibration activated.");
        }
#elif UNITY_ANDROID
        m_canVibrate = ResolveAndroidDevice();
        LogMessage("no Iphone device. Haptic vibration desactivated.");
#endif

        LogMessage("Haptics status on current device : " + m_canVibrate);
    }

    private bool ResolveAndroidDevice()
	{
        return SystemInfo.supportsVibration;
	}


    public void Vibrate()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        if ( m_canVibrate )
            MMVibrationManager.Vibrate();
#endif
        LogMessage("Standard vibration");
    }

    public void SetHapticsActive( bool active )
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        MMVibrationManager.SetHapticsActive(active);
#endif
    }

    public void StartContinuousHaptic(float intensity, float sharpness, float duration)
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        MMVibrationManager.ContinuousHaptic(intensity, sharpness, duration);
#endif
    }

    public void StartCustomContinuousHaptic(float interval = 0.15f)
    {
        InvokeRepeating("HapticLightImpact", 0f, interval );
    }

    public void StopContinuousHaptic()
    {
        CancelInvoke("HapticLightImpact");
#if MOREMOUNTAINS_NICEVIBRATIONS
        MMVibrationManager.StopContinuousHaptic();
        #endif
    }

    public void TransientHaptic(float intensity, float sharpness)
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        MMVibrationManager.TransientHaptic(intensity, sharpness);
#endif
    }

    public void CancelVibration()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        MMVibrationManager.StopContinuousHaptic();
#endif
    }

    public void HapticSelection()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        CallVibration(HapticTypes.Selection);
#endif
    }

    public void HapticSuccess()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        CallVibration(HapticTypes.Success);
#endif
    }

    public void HapticWarning()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        CallVibration(HapticTypes.Warning);
#endif
    }

    public void HapticFailure()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        CallVibration(HapticTypes.Failure);
#endif
    }


    public void HapticLightImpact()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        CallVibration(HapticTypes.LightImpact);
#endif
    }

    public void HapticMediumImpact()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        CallVibration(HapticTypes.MediumImpact);
#endif
    }


    public void HapticHeavyImpact()
    {
#if MOREMOUNTAINS_NICEVIBRATIONS
        CallVibration(HapticTypes.HeavyImpact);
#endif
    }

    private void LogMessage(string message)
	{
		if (m_verbose)
		{
			Debug.Log("[Haptic Manager] " + message);
		}
	}
#if MOREMOUNTAINS_NICEVIBRATIONS
	private void CallVibration(HapticTypes type)
	{

		if (m_canVibrate)
			MMVibrationManager.Haptic(type);


    LogMessage("Haptic vibration : " + type.ToString());
    }
#endif
}
