﻿using UnityEngine;
using System.Collections;

public class UIManager : SingletonMB<UIManager>
{
    [SerializeField]
    private Canvas m_targetCanvas = null ;

    [SerializeField]
    private StateToViewLink[] m_autolinkedViews = null;

    private AbstractView[] m_views;

    private AbstractView m_currentView;
    private UIView? m_nextViewId;
    private object m_nextViewParameters;

    protected override void Awake()
    {
        base.Awake();

        if ( m_targetCanvas == null )
        {
            m_targetCanvas = FindObjectOfType<Canvas>(); 
        }

        if ( m_targetCanvas == null )
        {
            Debug.LogWarning("Target Canvas not Specified for UIManager and no canvas found in scene");
        }


        m_views = m_targetCanvas.GetComponentsInChildren<AbstractView>(true);

        StateManager.Instance.OnGameStateChanged += OnGameStateChanged; 
    }

    public void OpenView(UIView view, object parameters = null)
    {

       if (m_currentView != null && m_currentView.Id == view)
            return;

        m_nextViewId = view;
        m_nextViewParameters = parameters;

        if ( m_currentView != null )
        {
            CloseCurrentView();
            return;
        }

        m_currentView = GetView((UIView)m_nextViewId) ;

        if (m_currentView != null)
        {
            m_currentView.OnViewClosed += OnCurrentViewClosed;
            m_nextViewId = null;
            m_currentView.Open(m_nextViewParameters);
        }

        else 
            Debug.LogWarning("Unknown view : " + m_nextViewId.ToString());
    }

    public void OpenViewAdditive(UIView view, object parameters = null)
    {
        AbstractView v = GetView(view);

        if ( v== null )
        {
            Debug.LogWarning("Unknown view : " + m_nextViewId.ToString());
        }
        else
        {
            if (!v.IsOpened)
                v.Open(parameters);
        }
    }

    public void CloseView(UIView view)
    {
        AbstractView v = GetView(view);

        if (v == null)
        {
            Debug.LogWarning("Unknown view : " + m_nextViewId.ToString());
        }
        else
        {
            if (v.IsOpened)
                v.Close();
        }
    }


    public void CloseCurrentView()
    {
        if (m_currentView == null)
            return;

        m_currentView.Close();

    }

    private void OnCurrentViewClosed()
    {
        m_currentView.OnViewClosed -= OnCurrentViewClosed;
        m_currentView = null;

        if (m_nextViewId != null)
            OpenView((UIView)m_nextViewId);
    }

    public AbstractView GetView(UIView id)
    {
        foreach ( AbstractView view in m_views )
        {
            if ( view.Id == id )
            {
                return view;
            }
        }

        return null; 
    }

    private void OnDestroy()
    {
        if (m_currentView != null)
            m_currentView.OnViewClosed -= OnCurrentViewClosed;

        if (StateManager.Instance != null)
            StateManager.Instance.OnGameStateChanged -= OnGameStateChanged;

    }

    private void OnGameStateChanged(GameState gameState)
    {
        foreach ( StateToViewLink l in m_autolinkedViews )
        {
            if ( l.m_gameState == gameState)
            {
                OpenView(l.m_view);
                return;
            }
        }
    }
}


[System.Serializable]
public class StateToViewLink
{
    public GameState m_gameState;
    public UIView m_view;
}