﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SoundManager : SingletonMB<SoundManager>
{
    public SoundAsset[] m_soundList;
    private Dictionary<SoundId, GameObject> m_audioSources;

    private bool m_active = true ;

    public void Start()
    {
        BuildSources(); 
    }

    public void SetSoundActive(bool active)
    {
        m_active = active; 
    }

    public void PlaySound(SoundId id)
    {
        if (!m_active)
            return;

        PlaySound(id, 1f);
    }

    public void PlaySound(SoundId id, float volume )
    {
        if (!m_active)
            return;

        SoundAsset s = GetSoundById(id);

        if (s != null)
        {
            AudioSource source = GetFreeAudioSource(s.id);

            if ( source != null )
            {
                source.volume = s.volume * volume;
                source.loop = s.loop;
                source.Play();
            }
        }
        else
        {
            Debug.LogWarning("SoundAsset " + id + " not registered");
        }
    }

    public void StopSound(SoundId id)
    {
        foreach (AudioSource source in m_audioSources[id].GetComponents<AudioSource>())
        {
            if (source.isPlaying )
            {
                source.Stop();
            }
        }
    }

    private void BuildSources()
    {
        m_audioSources = new Dictionary<SoundId, GameObject>(); 

        foreach ( SoundAsset s in m_soundList )
        {
            GameObject go = new GameObject(s.id.ToString());
            go.transform.parent = transform;

            for ( int i = 0; i < s.maxSources; ++i)
            {
                AudioSource source = go.AddComponent<AudioSource>();
                source.volume = s.volume;
                source.playOnAwake = false;
                source.loop = s.loop;
                source.clip = s.clip; 
            }

            m_audioSources.Add(s.id, go);
        }
    }

    private SoundAsset GetSoundById(SoundId id)
    {
        foreach (SoundAsset s in m_soundList)
        {
            if (s.id == id)
                return s;
        }

        return null;
    }

    private AudioSource GetFreeAudioSource( SoundId id)
    {

        foreach ( AudioSource source in m_audioSources[id].GetComponents<AudioSource>() )
        {
            if ( source.isPlaying == false )
            {
                return source; 
            }
        }

        return null; 
    }
}

[System.Serializable]
public class SoundAsset
{
    public SoundId id;
    public AudioClip clip;
    public float volume = 1.0f;
    public int maxSources = 1;
    public bool loop = false; 
}
