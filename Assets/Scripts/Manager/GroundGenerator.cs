﻿using Assets.Scripts.Water;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeGround
{
	empty,
	Color,
	Obstacle
}

#pragma warning disable CS0618
public class GroundGenerator : SingletonMB<GroundGenerator>
{
	[SerializeField] private Camera mainCamera = null;
	[SerializeField] private Transform m_StartPoint = null;
	[SerializeField] private GroundTile m_TilePrefab = null;
	[SerializeField] private float m_MovingSpeed = 12;
	[SerializeField] private int m_TilesToPreSpawn = 15;
	[SerializeField] private int m_TilesWithoutObstacles = 3;
	[SerializeField] private List<TriggerLD> m_ListObstacle = null;
	[SerializeField] private List<TriggerLD> m_ListColor = null;
	[SerializeField] private GameObject m_FinishLine = null;
	[SerializeField] private WaterPropertyBlockSetter m_WaterProperty = null;

	[Header("LD")]
	[SerializeField] private int m_NbrPerLevel = 20;
	[SerializeField] private int m_NbrMinBetweenEmpty = 2;
	[SerializeField] private int m_NbrMinBetweenObstacle = 2;

	[Header("Water")]
	[SerializeField] private float m_TimeFadeSpeedWater = 0.5f;
	[SerializeField] private float m_SpeedNoMoveWater = 0.2f;
	[SerializeField] private float m_SpeedRunWater = 2f;

	[Header("FxSpeed")]
	[SerializeField] private ParticleSystem m_ParticleSystem = null;
	[SerializeField] private float m_ParticleSystemSpeed = 48.7f;
	[SerializeField] private float m_ParticleSystemSpeedIncrease = 2;
	[SerializeField] private float m_ParticleSystemSpeedDecrease = 1;

	private int m_NbrTilesSpawn;
	private List<GroundTile> m_spawnedTiles;
	private List<TriggerLD> m_ListObstacleLevel;
	private List<TriggerLD> m_ListColorLevel;

	private int m_NbrBetweenEmpty;
	private int m_NbrBetweenObstacle;

	private void RestartGame()
	{
		m_NbrTilesSpawn = 0;
		m_ParticleSystem.startSpeed = 0;
		m_NbrBetweenEmpty = m_NbrMinBetweenEmpty;
		m_NbrBetweenObstacle = m_NbrMinBetweenObstacle;

		if (m_spawnedTiles != null)
		{
			foreach (var it in m_spawnedTiles)
			{
				Destroy(it.gameObject);
			}
			m_spawnedTiles.Clear();
		}
		if (m_ListObstacleLevel != null)
		{
			m_ListObstacleLevel.Clear();
		}
		if (m_ListObstacleLevel != null)
		{
			m_ListObstacleLevel.Clear();
		}
		m_WaterProperty.SetMoveDirectionWater(m_SpeedNoMoveWater);
	}

	public void Init()
	{
		RestartGame();
		UpdateListObstacle();

		m_spawnedTiles = new List<GroundTile>();
		Vector3 spawnPosition = m_StartPoint.position;

		for (int i = 0; i < m_TilesToPreSpawn; i++)
		{
			spawnPosition -= m_TilePrefab.GetStartPoint().localPosition;
			GroundTile spawnedTile = Instantiate(m_TilePrefab, spawnPosition, Quaternion.identity) as GroundTile;

			if (i < m_TilesWithoutObstacles)
			{
				spawnedTile.Init(null, m_WaterProperty);
			}
			else
			{
				spawnedTile.Init(GetRandomPrefabObstacle(), m_WaterProperty);
			}

			spawnPosition = spawnedTile.GetEndPoint().position;
			spawnedTile.transform.SetParent(transform);
			m_spawnedTiles.Add(spawnedTile);
			m_NbrTilesSpawn++;
		}
	}

	private void UpdateListObstacle()
	{
		int m_CurLevel = SaveManager.Instance.GetLevel();
		m_ListObstacleLevel = new List<TriggerLD>();
		m_ListColorLevel = new List<TriggerLD>();

		foreach (var it in m_ListObstacle)
		{
			if (it.m_MinLevelAppear <= m_CurLevel)
				m_ListObstacleLevel.Add(it);
		}

		foreach (var it in m_ListColor)
		{
			if (it.m_MinLevelAppear <= m_CurLevel)
				m_ListColorLevel.Add(it);
		}
	}



	private GameObject GetRandomPrefabObstacle()
	{
		List<TypeGround> tmpList = new List<TypeGround>();
		tmpList.Add(TypeGround.Color);

		if (m_NbrBetweenEmpty >= m_NbrMinBetweenEmpty)
			tmpList.Add(TypeGround.empty);
		if (m_NbrBetweenObstacle >= m_NbrMinBetweenObstacle)
			tmpList.Add(TypeGround.Obstacle);

		int rdm = Random.Range(0, tmpList.Count);
		switch (tmpList[rdm])
		{
			case TypeGround.Color:
				m_NbrBetweenEmpty += 1;
				m_NbrBetweenObstacle += 1;
				int rdmColor = Random.Range(0, m_ListColorLevel.Count);
				return m_ListColorLevel[rdmColor].gameObject;
			case TypeGround.Obstacle:
				m_NbrBetweenEmpty += 1;
				m_NbrBetweenObstacle = 0;
				int rdmObstacle = Random.Range(0, m_ListObstacleLevel.Count);
				return m_ListObstacleLevel[rdmObstacle].gameObject;
			case TypeGround.empty:
				m_NbrBetweenObstacle += 1;
				m_NbrBetweenEmpty = 0;
				return null;
		}

		return null;
	}

	void Update()
	{
		if (StateManager.Instance.CurrentState != GameState.GAME)
			return;

		transform.Translate(-m_spawnedTiles[0].transform.forward * Time.deltaTime * (m_MovingSpeed * Player.Instance.m_CurSpeed), Space.World);

		if (mainCamera.WorldToViewportPoint(m_spawnedTiles[0].GetEndPoint().position).z < 0)
		{
			GroundTile tileTmp = m_spawnedTiles[0];
			m_spawnedTiles.RemoveAt(0);
				tileTmp.transform.position = m_spawnedTiles[m_spawnedTiles.Count - 1].GetEndPoint().position - tileTmp.GetStartPoint().localPosition;

			if (m_NbrTilesSpawn == m_NbrPerLevel)
			{
				tileTmp.Init(m_FinishLine, m_WaterProperty);
			}
			else if (m_NbrTilesSpawn > m_NbrPerLevel)
			{
				tileTmp.Init(null, m_WaterProperty);
			}
			else
			{
				tileTmp.Init(GetRandomPrefabObstacle(), m_WaterProperty);
			}

			m_spawnedTiles.Add(tileTmp);
			m_NbrTilesSpawn++;
		}
	}



	public void StartRun()
	{
		m_WaterProperty.SetMoveDirectionWaterFade(m_SpeedRunWater, m_TimeFadeSpeedWater);
		m_ParticleSystem.gameObject.SetActive(true);

		DOTween.To(() => m_ParticleSystem.startSpeed, x => m_ParticleSystem.startSpeed = x, m_ParticleSystemSpeed, m_ParticleSystemSpeedIncrease);
	}

	public void StopRun()
	{
		m_WaterProperty.SetMoveDirectionWaterFade(m_SpeedNoMoveWater, m_TimeFadeSpeedWater);
		m_ParticleSystem.gameObject.SetActive(false);
		DOTween.To(() => m_ParticleSystem.startSpeed, x => m_ParticleSystem.startSpeed = x, 0, m_ParticleSystemSpeedDecrease);
	}
}