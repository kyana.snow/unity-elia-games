using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

#pragma warning disable 0649

public class CameraManager : SingletonMB<CameraManager>
{
    [SerializeField]
    private Camera m_targetCamera;

    [SerializeField]
    private Transform m_virtualCameras;

    [SerializeField]
    private StateToVCamLink[] m_autolinkedCameras = null;

    private CinemachineVirtualCamera[] m_vCams;
    private CinemachineBrain m_cameraBrain;
    private CinemachineVirtualCamera m_currentVCam;

    protected override void Awake()
    {
        base.Awake();

        if (m_targetCamera == null)
            m_targetCamera = Camera.main;

        m_cameraBrain = m_targetCamera.GetComponent<CinemachineBrain>();

        if (m_virtualCameras != null)
        {
            m_vCams = m_virtualCameras.GetComponentsInChildren<CinemachineVirtualCamera>(true);
        }

        StateManager.Instance.OnGameStateChanged += OnGameStateChanged;
    }

    public CinemachineVirtualCamera GetVirtualCamera(string name)
    {
        foreach (CinemachineVirtualCamera vCam in m_vCams)
        {
            if (vCam.name == name)
                return vCam;
        }

        return null;
    }

    public void SwitchToVirtualCamera(string name)
    {
        if (m_vCams == null)
        {
            Debug.LogWarning("No Virtual Cameras root defined");
            return;
        }

        foreach (CinemachineVirtualCamera vCam in m_vCams)
        {
            if (vCam.name == name)
            {
                vCam.Priority = 10;
                vCam.gameObject.SetActive(true);
                m_currentVCam = vCam;
            }

            else
            {
                vCam.Priority = 0;
                //vCam.gameObject.SetActive(false);
            }
        }
    }

    public CinemachineBrain GetBrain()
    {
        return m_cameraBrain;
    }

    public CinemachineVirtualCamera GetCurrentCamera()
    {
        return m_currentVCam;
    }

    public bool IsCinemachine()
    {
        return m_cameraBrain != null && m_cameraBrain.enabled;
    }


    private void OnGameStateChanged(GameState gameState)
    {
        foreach (StateToVCamLink link in m_autolinkedCameras)
        {
            if (link.m_gameState == gameState)
            {
                SwitchToVirtualCamera(link.m_virtualCamera.name);
            }
        }
    }


    public void Shake(float duration, float amplitude)
    {
        if (IsCinemachine() == false)
        {
            StartCoroutine(ApplyShake(duration, amplitude));
        }
        else
        {
            StartCoroutine(ApplyCinemachineShake(duration, amplitude));
        }
    }

    private IEnumerator ApplyCinemachineShake(float duration, float amplitude)
    {
        Noise(1, amplitude);
        yield return new WaitForSeconds(duration);
        Noise(0, 0);
    }

    private void Noise(float amplitudeGain, float frequencyGain)
    {
        foreach (CinemachineVirtualCamera vc in m_vCams)
        {
            CinemachineBasicMultiChannelPerlin noise = vc.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            if (noise != null)
            {
                noise.m_AmplitudeGain = amplitudeGain;
                noise.m_FrequencyGain = frequencyGain;
            }
        }
    }


    private IEnumerator ApplyShake(float duration, float amplitude)
    {

        float time = 0f;
        Vector3 startPosition = m_targetCamera.transform.localPosition;

        while (time < duration)
        {
            Vector3 move = new Vector3(Random.value * 2f - 1f, Random.value * 2f - 1f, Random.value * 2f - 1f);
            move = move.normalized * amplitude;
            m_targetCamera.transform.localPosition = startPosition + move;
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        m_targetCamera.transform.localPosition = startPosition;
    }
}



[System.Serializable]
public class StateToVCamLink
{
    public GameState m_gameState;
    public CinemachineVirtualCamera m_virtualCamera;
}
