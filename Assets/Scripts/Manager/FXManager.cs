﻿using UnityEngine;
using System.Collections;


public class FXManager : SingletonMB<FXManager>
{
    public ParticleData[] m_particlesList;

    protected override void Awake()
    {
        base.Awake(); 
    }

    public ParticleSystem CreateParticle(ParticleId id, Vector3 position, bool selfDestruct = true)
    {
        ParticleSystem prefab = GetParticlePrefab(id);

        if (prefab == null)
            return null;

        ParticleSystem system = Instantiate<ParticleSystem>(prefab, position, Quaternion.identity, transform);

        if ( selfDestruct )
        {
            ParticleSystem.MainModule main = system.main;
            main.stopAction = ParticleSystemStopAction.Destroy;
        }

        return system;
    }

    public ParticleSystem CreateParticle(ParticleId id, Transform parent, Vector3 localPosition, bool selfDestruct = true)
    {
        ParticleSystem prefab = GetParticlePrefab(id);

        if (prefab == null)
            return null;

        ParticleSystem system = Instantiate<ParticleSystem>(prefab);
        system.transform.SetParent(parent, false);
        system.transform.localPosition = localPosition;
        system.transform.localScale = Vector3.one;
        system.transform.localRotation = Quaternion.identity;

        if (selfDestruct)
        {
            ParticleSystem.MainModule main = system.main;
            main.stopAction = ParticleSystemStopAction.Destroy;
        }

        return system;
    }

    private ParticleSystem GetParticlePrefab(ParticleId id)
    {
        foreach (ParticleData p in m_particlesList)
        {
            if (p.m_id == id)
            {
                return p.m_prefab;
            }
        }

        return null; 
    }
}

[System.Serializable]
public class ParticleData
{
    public ParticleId m_id;
    public ParticleSystem m_prefab; 
}
