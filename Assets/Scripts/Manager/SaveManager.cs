using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : SingletonMB<SaveManager>
{
    [Tooltip("Usefull to force a save reset in builds")]
    [SerializeField]
    private string m_saveVersion = "1.0";


    [Tooltip("Clear save on play in editor mode")]
    [SerializeField]
    private bool m_clearSave = false;

    private const string c_LevelSave = "Level";
    private const string c_GameCount = "GameCount";
    private const string c_LastScore = "LastScore";
    private const string c_HighScore = "HighScore";
    private const string c_PlayerName = "PlayerName";
    private const string c_LevelTries = "LevelTries";
    private const string c_SoundEnabled = "SoundEnabled";
    private const string c_HapticsEnabled = "HapticsEnabled";

    protected override void Awake()
    {
        base.Awake();


#if UNITY_EDITOR
        if (m_clearSave)
            ClearSave();
#endif
    }


    private void Start()
    {

        if (PlayerPrefs.GetString("v") != m_saveVersion)
            ClearSave();


        MobileHapticManager.Instance.SetHapticsActive(GetHapticsEnabled());
        SoundManager.Instance.SetSoundActive(GetSoundEnabled());
    }

    private void ClearSave()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetString("v", m_saveVersion);
    }

    public void ForceSave()
    {
        PlayerPrefs.Save(); 
    }

    #region Level 
    public void GoToNextLevel()
    {
        SetLevel(GetLevel() + 1);
    }

    public void GoToPrevLevel()
    {
        SetLevel(Mathf.Max(GetLevel() - 1, 1));
    }

    public void ForceLevel(int level)
    {
        SetLevel(level);
    }

    public int GetLevel()
    {
        return GetInt(c_LevelSave, 1);
    }

    private void SetLevel(int level)
    {
        SetInt(c_LevelSave, level);
    }
    #endregion


    #region Level Tries
    public int GetLevelTries()
    {
        return GetInt(c_LevelTries, 0);
    }

    public void AddLevelTry()
    {
        SetInt(c_LevelTries, GetInt(c_LevelTries, 0) + 1);
    }

    public void ResetLevelTries()
    {
        SetInt(c_LevelTries, 0);
    }

    #endregion

    #region GameCount

    public void CountNewGame()
    {
        SetGameCount(GetGameCount() + 1);
    }

    public int GetGameCount()
    {
        return GetInt(c_GameCount, 0);
    }

    private void SetGameCount(int gameCount)
    {
        SetInt(c_GameCount, gameCount);
    }
    #endregion


    #region LastScore

    public int GetLastScore()
    {
        return GetInt(c_LastScore, 0);
    }

    public void SetLastScore(int lastScore)
    {
        SetInt(c_LastScore, lastScore);
    }
    #endregion


    #region High Score

    public int GetHighScore()
    {
        return GetInt(c_HighScore, 0);
    }

    public void SetHighScore(int highScore)
    {
        SetInt(c_HighScore, highScore);
    }
    #endregion

    #region Player Name

    public string GetPlayerName()
    {
        return GetString(c_PlayerName, "Player");
    }

    public void SetPlayerName(string playerName)
    {
        SetString(c_PlayerName, playerName);
    }
    #endregion

    #region Sound

    public bool GetSoundEnabled()
    {
        return GetBool(c_SoundEnabled, true);
    }

    public void SetSoundEnabled(bool enabled)
    {
        SetBool(c_SoundEnabled, enabled);
    }
    #endregion

    #region Haptics

    public bool GetHapticsEnabled()
    {
        return GetBool(c_HapticsEnabled, true);
    }

    public void SetHapticsEnabled(bool enabled)
    {
        SetBool(c_HapticsEnabled, enabled);
    }
    #endregion


    #region general methods
    public string GetString(string id, string defaultValue = "")
    {
        if (PlayerPrefs.HasKey(id))
            return PlayerPrefs.GetString(id);
        else
        {
            SetString(id, defaultValue);
            return defaultValue;
        }
    }

    public void SetString(string id, string value)
    {
        PlayerPrefs.SetString(id, value);

     //   if (AnalyticsManager.Instance != null)
      //      AnalyticsManager.Instance.UpdateCustomDataBySavedName(id, value);
    }

    public int GetInt(string id, int defaultValue = 0)
    {
        if (PlayerPrefs.HasKey(id))
            return PlayerPrefs.GetInt(id);
        else
        {
            SetInt(id, defaultValue);
            return defaultValue;
        }
    }

    public void SetInt(string id, int value)
    {
        PlayerPrefs.SetInt(id, value);

      //  if (AnalyticsManager.Instance != null)
      //      AnalyticsManager.Instance.UpdateCustomDataBySavedName(id, value);
    }

    public bool GetBool(string id, bool defaultValue = false)
    {
        if (PlayerPrefs.HasKey(id))
            return PlayerPrefs.GetInt(id) == 1;
        else
        {
            SetBool(id, defaultValue);
            return defaultValue;
        }
    }

    public void SetBool(string id, bool value)
    {
        PlayerPrefs.SetInt(id, value ? 1 : 0);

    //    if (AnalyticsManager.Instance != null)
      //      AnalyticsManager.Instance.UpdateCustomDataBySavedName(id, value ? 1 : 0);
    }


    #endregion
}
