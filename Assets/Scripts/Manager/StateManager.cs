﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : SingletonMB<StateManager>
{
    public delegate void OnGameStateEvent(GameState gameState);
    public event OnGameStateEvent OnGameStateChanged;

    public GameState CurrentState { get; private set;  }

    private GameState m_previousState;

    public void ChangeState(GameState gameState)
    {
        m_previousState = CurrentState;
        CurrentState = gameState;
        OnGameStateChanged?.Invoke(gameState);
    }

    public void RollbackState()
    {
        ChangeState(m_previousState);
    }
}
