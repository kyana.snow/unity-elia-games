﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : SingletonMB<GameManager> 
{
	[SerializeField] private TextMeshProUGUI m_LevelText = null;
    private void Start()
    {
        StateManager.Instance.OnGameStateChanged += OnGameStateChanged;
        StateManager.Instance.ChangeState(GameState.MENU);
    }

    public void CompleteLevel( bool hasWon )
    {
        if ( hasWon )
            SaveManager.Instance.GoToNextLevel();

        StateManager.Instance.ChangeState(GameState.MENU);
    }

	private void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.MENU:
				Init();
				break;
            case GameState.GAME:
				GroundGenerator.Instance.StartRun();
				break;
            case GameState.SUCCESS:
				GroundGenerator.Instance.StopRun();
				break;
            case GameState.FAIL:
				GroundGenerator.Instance.StopRun();
				break;
        }
    }

	private void Init()
	{
		int curLevel = SaveManager.Instance.GetLevel();
		m_LevelText.SetText("Level " + curLevel.ToString());
		GroundGenerator.Instance.Init();
	}
}