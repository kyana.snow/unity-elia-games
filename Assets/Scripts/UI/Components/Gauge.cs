﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using TMPro;

public class Gauge : MonoBehaviour
{
    [SerializeField]
    [Range(0f,1f)]
    private float m_easing = 0.15f; 


    private Image m_background;
    private Image m_progress;
    private TextMeshProUGUI m_labelStart;
    private TextMeshProUGUI m_labelGoal;

    private float m_destinationValue;
    private float m_currentValue;

    private void Awake()
    {
        
        m_background = transform.Find("frontGauge").GetComponent<Image>();
        m_progress = transform.Find("contentGauge").GetComponent<Image>();


        if ( transform.Find("startText"))
            m_labelStart = transform.Find("startText").GetComponent<TextMeshProUGUI>();

        if (transform.Find("goalText"))
            m_labelGoal = transform.Find("goalText").GetComponent<TextMeshProUGUI>();

        Reset(); 
    }

    public void SetValue( float ratio, bool instant = false  )
    {
        ratio = Mathf.Clamp01(ratio);
        m_destinationValue = ratio;
        if (instant)
            m_currentValue = ratio;

        Repaint(); 
    }

    public void Reset()
    {
        m_destinationValue = m_currentValue = 0f;
        Repaint(); 
    }


    public void SetColors( Color background, Color progress, Color labels )
    {
        m_background.color = background;
        m_progress.color = progress;

        if (m_labelStart)
            m_labelStart.color = labels;
        if (m_labelGoal)
            m_labelGoal.color = labels;
    }

    public void SetLevel( int level )
    {
        if ( m_labelStart )
            m_labelStart.text = FormatLevel(level);
        if (m_labelGoal)
            m_labelGoal.text = FormatLevel(level+1);
    }

    private string FormatLevel( int level )
    {
        string output = level.ToString();
        if (output.Length < 2) output = "0" + output;
        return output;
    }


    private void Update()
    {
        if ( m_destinationValue != m_currentValue)
        {
            m_currentValue += (m_destinationValue - m_currentValue )* m_easing;
            Repaint(); 
        }
    }

    private void Repaint()
    {
        m_progress.fillAmount = m_currentValue;
    }
}
