﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class InstructionSign : MonoBehaviour
{
    private CanvasGroup m_group;

    [SerializeField]
    private float m_fadeTime = 0.25f; 

    private void Awake()
    {
        m_group = GetComponent<CanvasGroup>();
        m_group.alpha = 0f;
      //  gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        DOTween.Kill(m_group);
        m_group.DOFade(1f, m_fadeTime).SetEase(Ease.OutCubic);
    }

    public void Hide()
    {
        DOTween.Kill(m_group);
        m_group.DOFade(0f, m_fadeTime).SetEase(Ease.InCubic).SetEase(Ease.InCubic).OnComplete(() => gameObject.SetActive(false));
    }
}
