﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InGameView : AbstractView
{
    public void OnWinGameButtonClick()
    {
        StateManager.Instance.ChangeState(GameState.SUCCESS);
    }

    public void OnLoseGameButtonClick()
    {
        StateManager.Instance.ChangeState(GameState.FAIL);
    }

    protected override void OpenComplete()
    {
        base.OpenComplete();
    }
}