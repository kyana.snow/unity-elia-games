﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FailView : AbstractView 
{
    public void OnRetryGameButtonClicked()
    {
        GameManager.Instance.CompleteLevel(false);
    }
}