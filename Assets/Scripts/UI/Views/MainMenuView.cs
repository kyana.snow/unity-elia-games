﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuView : AbstractView
{

    public void OnLaunchGameButtonClicked()
    {
        StateManager.Instance.ChangeState(GameState.GAME);
    }
}