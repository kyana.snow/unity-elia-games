﻿

public class SuccessView : AbstractView 
{
    public void OnNextLevelButtonClick()
    {
        GameManager.Instance.CompleteLevel(true);
    }
}