﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class AbstractView : MonoBehaviour
{
    public delegate void OnViewEvent();
    public event OnViewEvent OnViewOpened ;
    public event OnViewEvent OnViewClosed;

    [SerializeField] protected UIView m_viewId;
    [SerializeField] protected float m_fadingTime = 0.5f;
    [SerializeField] protected float m_timeBeforeAppearance ;
    [SerializeField] protected float m_timeAfterDisappearance ;

    protected object m_parameters;

    public UIView Id
    {
        get { return m_viewId; }
    }

    protected CanvasGroup m_Group;

    protected bool m_bOpened;
    public bool IsOpened
    {
        get { return m_bOpened; }
    }

    protected virtual void Awake()
    {
        GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        m_Group = GetComponent<CanvasGroup>();
        Initialize();
        Enable(false);
        gameObject.SetActive(false);
    }

    protected virtual void Initialize()
    {

    }

    public virtual void Open(object parameters = null)
    {
        gameObject.SetActive(true);
        m_parameters = parameters;
        m_bOpened = true;
        StartCoroutine("Appear",true);
    }

    public virtual void Close()
    {
        StartCoroutine("Appear", false);
    }

    protected virtual void OpenComplete()
    {
        OnViewOpened?.Invoke();
    }

    protected virtual void CloseComplete()
    {
        m_bOpened = false;
        OnViewClosed?.Invoke();
        gameObject.SetActive(false);
    }

    protected virtual void Enable(bool enable)
    {
        m_Group.alpha = enable ? 1.0f : 0.0f;
        m_Group.interactable = enable;
        m_Group.blocksRaycasts = enable;
    }

    protected virtual IEnumerator Appear(bool appear)
    {
        Enable(false);

        if (appear) { yield return new WaitForSecondsRealtime(m_timeBeforeAppearance); }

        float time = 0.0f;
        while (time < 1.0f)
        {
            time += Time.unscaledDeltaTime / m_fadingTime;
            m_Group.alpha = Mathf.Lerp(appear ? 0.0f : 1.0f, appear ? 1.0f : 0.0f, time);
            yield return null;
        }

        if (appear)
        {
            Enable(true);
            OpenComplete();
        }
        else
        {
            yield return new WaitForSecondsRealtime(m_timeAfterDisappearance);
            CloseComplete();
        }
    }


}
