﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Coffee.UIExtensions;

[ExecuteInEditMode]
public class UIParticleTargetFollow : MonoBehaviour
{
    public RectTransform m_goal;

    private ParticleSystem m_system;
    private static ParticleSystem.Particle[] m_particles = new ParticleSystem.Particle[500];

    private float m_scale = 0f;

    [Range(0f, 1f)]
    public float m_startLifeRatio;

    public float m_magnetMultiplier = 1f; 
    
    void Update()
    {
        if (m_system == null)
            m_system = GetComponent<ParticleSystem>();

        var count = m_system.GetParticles(m_particles);

        if ( m_scale == 0f )
            m_scale = GetComponent<UIParticle>().scale;


        Vector3 particleGoal = transform.InverseTransformPoint(m_goal.position) / m_scale;


        for (int i = 0; i < count; i++)
        {
            var particle = m_particles[i];

            

            if ( 1f - particle.remainingLifetime / particle.startLifetime > m_startLifeRatio)
            {
                Vector3 dist = (particleGoal - particle.position).normalized * m_magnetMultiplier;
                particle.velocity += dist;
                m_particles[i] = particle;
            }

           
        }

        m_system.SetParticles(m_particles, count);
    }
}
