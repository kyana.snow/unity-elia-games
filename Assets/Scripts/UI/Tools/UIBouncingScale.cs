﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class UIBouncingScale : MonoBehaviour
{
    [SerializeField]
    private float m_bounceAmplitude = 1.1f;

    [SerializeField]
    private float m_bounceTime = 0.5f;

    [SerializeField]
    private float m_idleTime = 0.5f;

    private Vector3 m_startScale;


    private void Awake()
    {
        m_startScale = transform.localScale; 
    }

    private IEnumerator DoBounce()
    {
        float ht = m_bounceTime * 0.5f;

        while (true)
        {
            bool bounce = true; 

            if ( GetComponent<Button>() != null )
            {
                if ( GetComponent<Button>().interactable == false )
                {
                    bounce = false;
                }
            }

            transform.localScale = m_startScale;
            yield return new WaitForSeconds(m_idleTime);
            if ( bounce )
            {
                transform.DOScale(m_startScale * m_bounceAmplitude, ht).SetEase(Ease.Linear);
                yield return new WaitForSeconds(ht);
                transform.DOScale(m_startScale, ht).SetEase(Ease.Linear);
                yield return new WaitForSeconds(ht);
            }
          
        }
    }

    private void OnEnable()
    {
        StartCoroutine("DoBounce");
    }

    private void OnDisable()
    {
        StopCoroutine("DoBounce");
    }


}
